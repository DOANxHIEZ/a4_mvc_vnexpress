﻿using System.Web;
using System.Web.Mvc;

namespace A4_MVC_VNEXPRESS
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
